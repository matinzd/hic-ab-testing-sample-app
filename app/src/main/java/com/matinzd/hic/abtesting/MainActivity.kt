package com.matinzd.hic.abtesting

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.get
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var remoteConfig: FirebaseRemoteConfig

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        remoteConfig = Firebase.remoteConfig
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 360000
        }
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        remoteConfig.fetchAndActivate()
            .addOnCompleteListener(this) { task ->
                if(task.isSuccessful) {
                    val newValues = task.result
//                    Toast.makeText(this, "New values fetched! $newValues", Toast.LENGTH_SHORT).show()
                    hello_world_text.text = remoteConfig[HELLO_WORLD_TEXT_KEY].asString()
                    hello_world_text.setTextColor(Color.parseColor(remoteConfig[HELLO_WORLD_TEXT_COLOR_KEY].asString()))
                }
            }
    }

    companion object {

        private const val TAG = "MainActivity"

        // Remote Config keys
        private const val HELLO_WORLD_TEXT_KEY = "hello_world_text"
        private const val HELLO_WORLD_TEXT_COLOR_KEY = "hello_world_text_color"
        private const val WELCOME_MESSAGE_KEY = "welcome_message"
        private const val WELCOME_MESSAGE_CAPS_KEY = "welcome_message_caps"
    }
}
